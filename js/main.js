$(function() {
  $(".input-group input").focus();

  $("#btn-search").click(function() {
    toggleDisplay();
  });

  $("#results p>a").click(function(event) {
    event.preventDefault();
    event.stopPropagation();

    toggleDisplay();
    $(".input-group input").focus();
    $(".typeahead").typeahead("close");
    $(".input-group input").val("");
    clearResults();
  });

  var data = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: { url: "./data.json", cache: false }
  });

  $(".typeahead").typeahead(
    {
      highlight: true
    },
    {
      limit: 10,
      name: "data",
      display: "value",
      source: data
    }
  );

  $(".tt-dataset").on("click", function() {
    $("#btn-search").click();
  });

  $(".typeahead").bind("typeahead:select", function(e, suggestion) {
    showResults(true, suggestion.img, suggestion.text, suggestion.src);
  });

  $(".typeahead").on("keyup", function(e) {
    if (e.which !== 40 && e.which !== 38) {
      if ($(".tt-suggestion").is(":visible")) {
        $(".tt-suggestion")
          .first()
          .addClass("tt-cursor");
      }
    }

    if (e.which == 13) {
      if ($(".tt-suggestion").is(":visible")) {
        $(".tt-suggestion:first-child").click();
      }
      $("#btn-search").click();
    }

    if (e.which == 9) {
      $("#btn-search").click();
    }
  });
});

function clearResults() {
  $("#results h1").text("No!");
  $("#results p>img").attr("src", "images/1x1.png");
  $("#results p>img").removeClass("p-2");
  $(".results-text").text("");
  $(".results-src").text("");
}

function showResults(answer = false, image = "", text = "", src = "") {
  clearResults();

  if (answer) {
    $("#results h1").text("Yes!");
  }

  if (image) {
    $("#results p>img").attr("src", image);
    $("#results p>img").addClass("p-2");
  }

  if (text) {
    $(".results-text").text(text);
  }

  if (src) {
    $(".results-src").text("Source: " + src);
  }
}

function toggleDisplay() {
  $("#search").toggleClass("d-none");
  $("#results").toggleClass("d-none");
}
